import React, { useEffect, useState, useCallback} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Image,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Star from 'react-native-vector-icons/AntDesign';

import { useDispatch, useSelector } from 'react-redux';

import { booksList, popularList, getBookDetail } from '../Action';



export default function HomePage({navigation}){
    

    const tokenval = useSelector(state=>state.appData.tokenValue);
    console.log(tokenval);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(booksList(tokenval));
        dispatch(popularList(tokenval));
        setDone(true);
    },[]);


    const[refreshing, setRefreshing] = useState(false);
    const[done, setDone] = useState(false);

    const books = useSelector(state=>state.appData.bookList);
    console.log(books);

    const popBooks = useSelector(state=>state.appData.popBookList);
    console.log(popBooks);

    function bookClick(bookID){
        dispatch(getBookDetail(bookID, tokenval));
        setTimeout(() => {
            navigation.navigate('Book');
        }, 1000);
    }


    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }    
   
    const onRefresh = useCallback(()=>{
        setRefreshing(true);
        console.log('REFRESHING');
        wait(2000).then(()=>setRefreshing(false));
      }, []);

      function convertNumberToRupiah(input){
        let result = '';
        let rupiah = '';
        let tmpRev = input.toString().split('').reverse().join('');
        for(let i = 0; i < tmpRev.length; i++){
          if(i % 3 == 0){
            rupiah += tmpRev.substr(i,3)+'.';
          }
        }
        result = rupiah.split('',rupiah.length-1).reverse().join('');
        return result;
      }
    

    return(
        <>
            {
                done == false ?
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large" color="#ffffff" style={styles.loadingStyle}/>
                </View>
                :
                <View style={styles.mainContainer}>
                    <View style={styles.contentContainer}>
                        <Text style={{color: 'black', fontSize : 20}}>Good Morning Vega!</Text>
                        <View style={styles.recommendedContainer}>
                            <Text style={{color : 'black'}}>Recommended</Text>
                            <View style={styles.recommendList}>
                                <ScrollView horizontal={true}>
                                    {
                                    books.map(item =>{
                                            return(
                                                <TouchableOpacity key={item.id} onPress={() => bookClick(item.id)}>
                                                    <Image style={styles.imageRecommend} source={{uri : item.cover_image}}  key={item.id}/>
                                                </TouchableOpacity>
                                            );
                                    })
                                    }
                                </ScrollView>
                            </View>
                        </View>
                        <View style={styles.popularContainer}>
                            <Text style={{color : 'black'}}>Popular Book</Text>
                            <View style={styles.scrollContainer}>
                                <ScrollView RefreshControl={
                                    <RefreshControl 
                                        refreshing={refreshing}
                                        onRefresh={onRefresh}
                                    />
                                }>
                                   <View style={styles.popListContainer}>
                                    <View style={styles.popBookSort}>
                                         {
                                             popBooks.map(item => {
                                                 return(
                                                     <View style={styles.popBookContainer}>
                                                         <TouchableOpacity key={item.id} onPress={() => bookClick(item.id)}>
                                                             <Image style={styles.imageRecommend} source={{uri : item.cover_image}}  key={item.id}/>
                                                         </TouchableOpacity>
                                                         <Text style={{width : '100%', color : 'black'}}>{item.title}</Text>
                                                         <Text style={{width : '100%', color : 'black'}}>{item.author}</Text>
                                                         <Text style={{width : '100%', color : 'black'}}>{item.publisher}</Text>
                                                         <Text style={{width : '100%', color : 'black'}}> <Star name="star" style={{color : 'yellow'}} size={18}/>{item.average_rating}</Text>
                                                         <Text style={{width : '100%', color : 'black'}}>{`Rp. ${convertNumberToRupiah(item.price)}`}</Text>
                                                     </View>
                                                 );
                                             })
                                         }
                                     </View>
                                   </View>
                                </ScrollView>
                            </View>
                        </View>
                    </View>
                </View>
            }
        </>
    );
}

const styles = StyleSheet.create({
    mainContainer : {
        width : '100%',
        height : '100%',
        backgroundColor : '#e7e7e7'
    },
    contentContainer : {
        paddingTop : '5%',
        paddingLeft : '5%',
        paddingRight : '5%'
    },
    imageRecommend : {
        width : 100,
        height  : 155,
        marginRight : 10,
        marginTop : 10,
        marginBottom : 10 
    },
    recommendList : {
        paddingLeft : '1%',
        paddingRight : '1%'
    },
    popListContainer : {
        paddingLeft : '1%',
        paddingBottom : '20%',
        height : '20%'
    },  
    popBookSort : {
        flexDirection :'row',
        flexWrap : 'wrap',
        justifyContent : 'space-between',
    },
    popBookContainer :{
        flexWrap : 'wrap',
        width : '30%'
    },  
    scrollContainer : {
        height : '80%',
        paddingBottom : '3%'
    },
    loadingContainer : {
        backgroundColor: 'black',
        width : '100%',
        height : '100%',
        alignItems : 'center',
        justifyContent : 'center'
      },
});