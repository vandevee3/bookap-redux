import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import SuccesCheck from 'react-native-vector-icons/AntDesign';


export default function RegisterSuccess({navigation}){

    return(
        <View style={styles.mainContainer}>
            <View style={styles.contentContainer}>
                <View style={styles.succesContainer}>
                    <Text style={{color : 'black', fontSize : 20, textAlign : 'center'}}>Registration Complete</Text>
                    <Text style={{color : 'black', fontSize : 20, textAlign : 'center'}}>We sent email verification to your email!</Text>
                    <SuccesCheck name="checkcircle" size={200} color={'green'} style={{marginTop : '10%'}}/>
                </View>
                <View style={styles.buttonBack}>
                    <TouchableOpacity onPress={()=> navigation.navigate('Login')}>
                        <Text style={{color : 'black'}}> Back to Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    mainContainer : {
        width : '100%',
        height : '100%'
    },
    contentContainer : {
        paddingLeft : '8%',
        paddingRight : '8%',
        marginTop : '4%' ,
        justifyContent : 'space-between',
        height : '80%'
    },
    succesContainer : {
        flexDirection : 'column',
        alignItems : 'center',
        justifyContent : 'center',
        paddingTop : '30%'
    },
    buttonBack : {
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'white',
        height : '10%',
    }
});