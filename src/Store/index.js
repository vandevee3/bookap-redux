import { applyMiddleware, createStore, combineReducers } from "redux";
import thunk from "redux-thunk";

import Reducer from "../Reducer";

const Reducers = {
    appData : Reducer
}

export const store = createStore(combineReducers(Reducers), applyMiddleware(thunk));
